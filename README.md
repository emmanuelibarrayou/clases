Clases estáticas
Se considera clase estática aquella clase la cual todos sus miembros públicos son estáticos, qué quiere decir esto, que todos sus atributos y/o métodos públicos son estáticos.
Con esto no quiere decir que no pueda haber un poco de ambos, perfectamente puedes tener en tu implementación métodos o atributos públicos no estáticos, o mejor aún, métodos o atributos privados, pero el detalle está en el uso que le darás a los mismos.



Clases selladas

A través de una clase sellada restringimos el ámbito de herencia de un tipo de dato del modelo del mundo del problema. A través de esta restricción el programador está explicitando el bloqueo sobre la herencia de nuevos tipos de objeto que intenten heredar la funcionalidad y comportamiento del supertipo (es claro que ya no cabe este adjetivo para el tipo de dato en cuestión). En otros lenguajes de programación, por ejemplo, en Java, se utiliza la construcción sintáctica final para significar el mismo concepto de sellamiento (sí así se puede referenciar). Mientras que en Visual Basic NotInheritable.



Clases anidadas 
¿Qué es una clase anidada? Las clases anidadas te permiten agrupar lógicamente clases que solo se utilizan en un lugar, por lo tanto, esto aumenta el uso de la encapsulación y crea un código más fácil de leer y de mantener.
Una clase anidada no existe independientemente de su clase adjunta. Por lo tanto, el alcance de una clase anidada está limitado por su clase externa.
Una clase anidada también es miembro de su clase adjunta. También es posible declarar una clase anidada que es local a un bloque.
Como miembro de su clase adjunta, una clase anidada se puede declarar private, public, protected, o default.
Una clase anidada tiene acceso a los miembros, incluidos los miembros privados, de la clase en la que está anidado. Sin embargo, lo inverso no es verdadero, es decir, la clase adjunta no tiene acceso a los miembros de la clase anidada.
Hay dos tipos generales de clases anidadas: las que están precedidas por el modificador static (static nested class) y las que no lo están.
 


Clases parciales
Un caso de uso directo de una clase parcial es permitir que los generadores de código separen físicamente el código, pero mantengan (compilen) las partes discretas relacionadas lógicamente (el mismo nombre de clase). Por ejemplo, WinForms y WPF usan la función para separar el código generado en un archivo diferente. También Entity Framework o cualquier otro marco que admita la generación de código.
